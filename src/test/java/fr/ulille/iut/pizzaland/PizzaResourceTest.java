package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class PizzaResourceTest extends JerseyTest
{
	private PizzaDao dao;
	
	private static final Logger LOGGER = Logger.getLogger(IngredientResourceTest.class.getName());

	@Override
    protected Application configure() {
    	BDDFactory.setJdbiForTests();
    	
    	return new ApiV1();
    }
	
	@Before
    public void setEnvUp() {
    	dao = BDDFactory.buildDao(PizzaDao.class);
        dao.createTableAndIngredientAssociation();
    }

    @After
    public void tearEnvDown() throws Exception {
    	dao.dropTable();
    }
    
    @Test
    public void testGetEmptyList()
    {
    	Response response = target("/pizzas").request().get();
    	assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    	
    	List<PizzaDto> pizzas;
    	pizzas = response.readEntity(new GenericType<List<PizzaDto>>(){});
    	assertEquals(0, pizzas.size());
    }
    
    @Test
    public void testGetExistingPizza() {

    	Pizza pizza = new Pizza();
    	pizza.setName("Reine");
    	pizza.setBase("Tomate");
    	pizza.setPrice(new double[] {9.0, 12.0});

    	// Le probleme est au  niveau de l'insert
        long id = dao.insert(pizza.getName(), pizza.getBase(), pizza.getPrice()[0], pizza.getPrice()[1]);
        pizza.setId(id);

        Pizza pizzaTest = dao.findById(id); 
        System.out.println(pizzaTest);
        
        
        Response response = target("/pizzas/" + id).request().get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        PizzaDto pizzaDto = response.readEntity(PizzaDto.class);
        System.out.println(pizzaDto);
        Pizza result = Pizza.fromDto(pizzaDto);
        
        System.out.println("---------------------------");
        System.out.println(result.getName());
        System.out.println(pizza.getName());
        System.out.println("---------------------------");
        System.out.println(result.getBase());
        System.out.println(pizza.getBase());
        System.out.println("---------------------------");
        System.out.println(result.getPrice()[0]);
        System.out.println(pizza.getPrice()[0]);
        System.out.println("---------------------------");
        System.out.println(result.getPrice()[1]);
        System.out.println(pizza.getPrice()[1]);
        System.out.println("---------------------------");
        System.out.println(Arrays.equals(result.getPrice(), pizza.getPrice()));
        System.out.println("---------------------------");
        
        assertEquals(pizza, result);
    }
    
    @Test
    public void testGetPizzaName() {
        Pizza pizza = new Pizza();
        pizza.setName("Nordique");
        pizza.setBase("Creme");
        pizza.setPrice(new double[] {9.0, 12.0});
        
        long id = dao.insert(pizza.getName(), pizza.getBase(), pizza.getPrice()[0], pizza.getPrice()[1]);

        Response response = target("pizzas/" + id + "/name").request().get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        assertEquals("Nordique", response.readEntity(String.class));
    }
    
    @Test
    public void testGetNotExistingPizza() {
      Response response = target("/pizzas/125").request().get();
      assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
    }
    
    @Test
    public void testCreateIngredient() {
        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
        pizzaCreateDto.setName("Nordique");
        pizzaCreateDto.setBase("Creme");
        pizzaCreateDto.setPriceLittle(9.0);
        pizzaCreateDto.setPriceBig(12.0);

        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

        assertEquals(target("/pizzas/" +returnedEntity.getId()).getUri(), response.getLocation());
    	
        assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
    }
    
    @Test
    public void testCreateSamePizza() {
    	PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
        pizzaCreateDto.setName("Nordique");
        pizzaCreateDto.setBase("Creme");
        pizzaCreateDto.setPriceLittle(9.0);
        pizzaCreateDto.setPriceBig(12.0);
        
        dao.insert(pizzaCreateDto.getName(), pizzaCreateDto.getBase(), pizzaCreateDto.getPriceLittle(), pizzaCreateDto.getPriceBig());

        Response response = target("/pizzas")
                .request()
                .post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
    }
    
   @Test
    public void testCreatePizzaWithoutName() {
    	PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

        Response response = target("/pizzas")
                .request()
                .post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }
 
    @Test
    public void testDeleteExistingPizza() {
        Pizza pizza = new Pizza();
        pizza.setName("Nordique");
        pizza.setBase("Creme");
        pizza.setPrice(new double[] {9.0, 12.0});
        
        long id = dao.insert(pizza.getName(), pizza.getBase(), pizza.getPrice()[0], pizza.getPrice()[1]);
        pizza.setId(id);

        Response response = target("/pizzas/" + id).request().delete();

        assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

        Pizza result = dao.findById(id);
    	assertEquals(result, null);
    }

    @Test
    public void testDeleteNotExistingPizza() {
        Response response = target("/pizzas/125").request().delete();
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
    	response.getStatus());
    }
    
    @Test
    public void testGetNotExistingPizzaName() {
        Response response = target("pizzas/125/name").request().get();

        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreateWithForm() {
        Form form = new Form();
        form.param("name", "raclette");
        form.param("base", "creme");
        form.param("price_little", "9.0");
        form.param("price_big", "12.0");

        Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        Response response = target("pizzas").request().post(formEntity);
        
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
        String location = response.getHeaderString("Location");
        long id = Integer.parseInt(location.substring(location.lastIndexOf('/') + 1));
        Pizza result = dao.findById(id);
    	
        assertNotNull(result);
    }
}
