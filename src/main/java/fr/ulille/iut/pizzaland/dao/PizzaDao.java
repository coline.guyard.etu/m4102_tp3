package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlBatch;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

  @SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL, base VARCHAR UNIQUE NOT NULL, price_little INTEGER, price_big INTEGER)")
  void createPizzaTable();

  @SqlUpdate("CREATE TABLE IF NOT EXISTS pizzaIngredientsAssociation ("
  		+ "id_pizza INTEGER NOT NULL, "
  		+ "id_ingredient INTEGER NOT NULL, "
  		+ "CONSTRAINT pk_pizza PRIMARY KEY (id_pizza, id_ingredient), "
  		+ "FOREIGN KEY (id_pizza) REFERENCES pizzas (id), "
  		+ "FOREIGN KEY (id_ingredient) REFERENCES ingredients (id)"
	+ ")")
  void createAssociationTable();

  @Transaction
  default void createTableAndIngredientAssociation() {
    createAssociationTable();
    createPizzaTable();
  }
  
  @SqlUpdate("DROP TABLE IF EXISTS pizzas")
  void dropTable();
  
  @SqlUpdate("INSERT INTO pizzas (name, base, price_little, price_big) VALUES (:name, :base, :price_little, :price_big)")
  @GetGeneratedKeys
  long insert(String name, String base, double price_little, double price_big);
  
  @SqlBatch("INSERT INTO pizzaIngredientsAssociation (id_pizza, id_ingredient) VALUES (:id_pizza, :id)")
  void insertIngredient(long id_pizza, Iterable<Ingredient> ingredients);
  
  default long insertFullPizza(Pizza pizza) {
	long id = insert(pizza.getName(), pizza.getBase(), pizza.getPrice()[0], pizza.getPrice()[1]);
	insertIngredient(id, pizza.getIngredients());
	return id;
  }
  
  @SqlQuery("SELECT * FROM pizzas")
  @RegisterBeanMapper(Pizza.class)
  List<Pizza> getAll();
  
  @SqlQuery("SELECT * FROM pizzas WHERE id = :id")
  @RegisterBeanMapper(Pizza.class)
  Pizza findById(long id);
  
  @SqlQuery("SELECT * FROM pizzas WHERE name = :name")
  @RegisterBeanMapper(Pizza.class)
  Pizza findByName(String name);

  @SqlUpdate("DELETE FROM pizzas WHERE id = :id")
  void remove(long id);
}
