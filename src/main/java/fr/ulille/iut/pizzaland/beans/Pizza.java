package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza
{
	private long id;
	
	private String name;
	
	private String base;
	
	private List<Ingredient> ingredients;
	
	private double[] price;
	
	public Pizza(long id, String name, String base, List<Ingredient> ingredients, double[] price)
	{
		this.id = id;
		this.name = name;
		this.base = base;
		this.ingredients = ingredients;
		this.price = price;
	}
	
	public Pizza() {
		ingredients = new ArrayList<Ingredient>();
		price = new double[2];
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getBase()
	{
		return base;
	}

	public void setBase(String base)
	{
		this.base = base;
	}

	public List<Ingredient> getIngredients()
	{
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients)
	{
		this.ingredients = ingredients;
	}

	public double[] getPrice()
	{
		return price;
	}

	public void setPrice(double[] price)
	{
		this.price = price;
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((base == null) ? 0 : base.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((ingredients == null) ? 0 : ingredients.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Arrays.hashCode(price);
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (base == null) {
			if (other.base != null)
				return false;
		} else if (!base.equals(other.base))
			return false;
		if (id != other.id)
			return false;
		if (ingredients == null) {
			if (other.ingredients != null)
				return false;
		} else if (!ingredients.equals(other.ingredients))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (!Arrays.equals(price, other.price))
			return false;
		return true;
	}

	public static PizzaDto toDto(Pizza p) {
	    PizzaDto dto = new PizzaDto();
	    dto.setId(p.getId());
	    dto.setName(p.getName());
	    dto.setBase(p.getBase());
	    dto.setIngredients(p.getIngredients());
	    dto.setPrice(p.getPrice());
	    
	    return dto;
	}
	
	public static Pizza fromDto(PizzaDto dto) {
	    Pizza pizza = new Pizza();
	    pizza.setId(dto.getId());
	    pizza.setName(dto.getName());
	    pizza.setBase(dto.getBase());
	    System.out.println("->"+ dto.getName());
	    System.out.println("->"+ dto.getPrice()[0]);
	    System.out.println("->"+ dto.getPrice()[1]);
	    pizza.setPrice(new double[] {dto.getPrice()[0], dto.getPrice()[1]});
	    
	    return pizza;
	}
	
	public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
	    Pizza pizza = new Pizza();
	    pizza.setName(dto.getName());
	    pizza.setBase(dto.getBase());
	    pizza.setPrice(new double[] {dto.getPriceLittle(), dto.getPriceBig()});

	    return pizza;
	}

	@Override
	public String toString()
	{
		return "Pizza [id=" + id + ", name=" + name + ", base=" + base + ", ingredients=" + ingredients + ", price="
				+ Arrays.toString(price) + "]";
	}

}
