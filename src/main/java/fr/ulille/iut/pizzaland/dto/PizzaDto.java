package fr.ulille.iut.pizzaland.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaDto
{
	private long id;
	
	private String name;
	
	private String base;
	
	private List<Ingredient> ingredients;
	
	private double[] price;
	
	public PizzaDto()
	{
		ingredients = new ArrayList<Ingredient>();
		price = new double[2];
	}
	
	public long getId()
	{
		return id;
	}
	
	public void setId(long id)
	{
		this.id = id;
	}
	
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getBase()
	{
		return base;
	}

	public void setBase(String base)
	{
		this.base = base;
	}

	public double[] getPrice()
	{
		return price;
	}

	public void setPrice(double[] price)
	{
		this.price = price;
	}

	public List<Ingredient> getIngredients()
	{
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients)
	{
		this.ingredients = ingredients;
	}

	@Override
	public String toString()
	{
		return "PizzaDto [id=" + id + ", name=" + name + ", base=" + base + ", ingredients=" + ingredients + ", price="
				+ Arrays.toString(price) + "]";
	}

	
}
