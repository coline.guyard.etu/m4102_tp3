package fr.ulille.iut.pizzaland.dto;

public class PizzaCreateDto
{
	private String name;
	private String base;
	
	private double priceLittle;
	private double priceBig;
	
	public PizzaCreateDto() {}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}

	public String getBase()
	{
		return base;
	}

	public void setBase(String base)
	{
		this.base = base;
	}

	public double getPriceLittle()
	{
		return priceLittle;
	}

	public void setPriceLittle(double priceLittle)
	{
		this.priceLittle = priceLittle;
	}

	public double getPriceBig()
	{
		return priceBig;
	}

	public void setPriceBig(double priceBig)
	{
		this.priceBig = priceBig;
	}

}
